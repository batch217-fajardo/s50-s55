import { Navigate } from 'react-router-dom'
import { useContext, useEffect } from 'react'
import UserContext from '../UserContext.js'

export default function Logout(){

	// localStorage.clear();
	const {unsetUser, setUser} = useContext(UserContext);
	// clears the localStorage where the user information is stored. 
	unsetUser();

	// just to be sure that the info is cleared setUser email to null
	useEffect(() => {
		setUser({email:null})
	})

	return(

		<Navigate to="/login"/>

		)
}