import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js'
import { Navigate } from 'react-router-dom'


export default function Login(){

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(true)

	useEffect(()=>{
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false)
		}
	})

	function authenticate(e){
		e.preventDefault()

		// once user successfully logged in, the email will be stored in the localStorage of the browser
		localStorage.setItem("email" , email)

		// once browser localStorage has 1 email stored, setUser will send the data in to the userContext which can be used for data passing on all pages
		setUser({
			email: localStorage.getItem("email")
		})

		setEmail('');
		setPassword('');

		console.log(`${email} has been verified! Welcome back!`)
	}


	return(
		
		// since the ternary is the parent no need for curly braces

		//once user is authenticated, if successful, user will be directed to the courses page, else user will stay in the login page.
		(user.email) ?
		<Navigate to="/courses"/>
		:
		<>
			<h1>Login</h1>

			<Form onSubmit={(e) => authenticate(e)}>
		{/*EMAIL SECTION*/}
		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" required onChange={e => setEmail(e.target.value)} value = {email} />
		      </Form.Group>

		  {/*PASSWORD SECTION*/}
		      <Form.Group className="mb-3" controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" required onChange={e => setPassword(e.target.value)} value={password} />
		      </Form.Group>

		  {/*SUBMIT BUTTON*/}

		  {
		  	isActive ?

		  	<Button variant="primary" type="submit">
		        Login
		    </Button>

		    :
		    <Button variant="primary" type="submit" disabled>
		        Login
		     </Button>

		  }  
	    	</Form>
    	</>

		)
}