import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js'
import { Navigate } from 'react-router-dom'

export default function Register(){

	// accepting the passed data from UserContext
	const {user} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	})

	function registerUser(e){
		e.preventDefault();

		setEmail('');
		setPassword1('');
		setPassword2('');
		alert("Thank you for registering!");
	}

	return (

		(user.email !== null) ?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Register</h1>

		<Form onSubmit={(e) => registerUser(e)}>
	{/*EMAIL SECTION*/}
	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" required onChange={e => setEmail(e.target.value)} value = {email} />
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	  {/*PASSWORD 1 AND 2 SECTION*/}
	      <Form.Group className="mb-3" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" required onChange={e => setPassword1(e.target.value)} value={password1} />
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" required onChange={e => setPassword2(e.target.value)} value={password2} />
	      </Form.Group>

	  {/*SUBMIT BUTTON*/}

	  {
	  	isActive ?

	  	<Button variant="primary" type="submit">
	        Submit
	    </Button>

	    :
	    <Button variant="primary" type="submit" disabled>
	        Submit
	     </Button>

	  }  
    	</Form>
    	</>

		)
}