import React from 'react'

// usercontext holds all the informtion
const UserContext = React.createContext();

// provides the data
export const UserProvider = UserContext.Provider;

export default UserContext;